# Tarea01
Damian Rivera Gonzalez

Componentes de la computadora.

CPU: Intel (R) Pentium(R) CPU 997 a 1.60 GHz, versión 6.10.7, tamaño 907 MHz, Capacidad 4GHz
	Anchura 64 bits, Reloj 100 MHz, Arquitectura: x86_64
	Configuración: 2 núcleos, 2 hablitados, 2 hilos.

Memoria RAM: Memoria del sistema 2GB, placa base.

Disco duro: ATA Disk, Seagate ST500LT012-9WS14
	Nombre lógico; /dev/sda Tamaño: 465 GB (500 GB) Dos particiones.

Tarjeta de Red: Ethernet interface, RTL8101/2/6E PCI Express Fast/Gigabit Ethernet controller
	Tamaño 100 Mb/s, Capacidad 100 Mb/s, anchura 64 bits, reloj 33 MHz
	Capacidades: pm msi pciexpress msix vpd bus_master cap_list ethernet physical tp mii 10bt 10bt-fd 100bt 100bt-fd autonegotiation

Trajeta Madre: Type2 - Board Chassis Location.

L1 cache: tamaño 32KiB, capacidad 32KiB, capacidades; internal write-through instruction, nivel- 1
L2 cache: tamaño 256KiB, capacidad: 256KiB, capacidades: internal write-through unified, nivel- 2
L3 cache: tamaño 2MiB, capacidad 2MiB, capacidades; internal write-back unified, level- 3

Monitor: VGA compatible controller, 2nd Generation Core Processor Family Integrated Graphics Controller, Intel Corporation, 
	anchura: 64 bits, reloj: 33MHz, capacidades: msi pm vga_controller bus_master cap_list rom

USB controller: 7 Series/C210 Series Chipset Family USB xHCI Host Controller, Intel Corporation, 
	anchura: 64 bits, reloj: 33MHz, capacidades: pm msi xhci bus_master cap_list

Communication controller: 7 Series/C216 Chipset Family MEI Controller #1, Intel Corporation,
	anchura: 64 bits, reloj: 33MHz, capacidades: pm msi bus_master cap_list

Multimedia: Audio device, 7 Series/C216 Chipset Family High Definition Audio Controller, Intel Corporation, 
	anchura 64 bits, reloj: 33MHz, capacidades pm msi pciexpress bus_master cap_list



Comandos Output.

lsblk: lista a informacion sobre todos los bloques de dispositivos disponibles o especificados.
	--all : se muestran los dispositivos vacios, que son omitidos por defecto
	--fs : muestra información sobre los sistemas de archivos.
	--paths : muestra información sobre todos los caminos a los dispositivos

lshw: muestra información sobre la configuración del hardware en la máquina.
	-businfo : muestra la lista de información de los canales de los dispositivos
	-html : muestra la información con una estructura en html
	-short : genera el árbol de dispositivos mostrados en los caminos del hardware

lspci: muestra información sobre los canales PCI en el sistema y como se conectan con los dispositivos.
	-v : muestra con mayor detalle la información de los dispositivos
	-t : muestra un diagrama en forma de árbol de todos los canales, puentes y dispositivos conectados entre si.
	-vv : muestra aun con mayor detalle la información, incluyendo información que se considera útil.

lsusb: muestra la información de todos los canales de USB en el sistema y como están conectados entre si
	-v : muestra con mayor detalle la información de los dispositivos.
	-t : muestra la información de la jerarquía de los dispositivos de USB.

smartctl: controla la tecnología de autocontrol que se encuentra integrado en los discos duros ATA/SATA y SCSI/SAS y discos de estado sólido.
	monitorea la confiabilidad del disco duro y predice fallas del disco, todo con respecto al dispositivo especificado
	--all : imprime toda la información SMART del disco 
	--scan : escanea los dispositivos e imprime el nombre de cada dispositivo junto con la información de su protocolo 
